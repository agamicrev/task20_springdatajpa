package com.agamicrev.DTO;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.agamicrev.controller.BookController;
import com.agamicrev.domain.Customer;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class CustomerDTO extends ResourceSupport {

  Customer customer;

  public CustomerDTO(Customer customer, Link selfLink)
      throws NoSuchCustomerException, NoSuchBookException {
    this.customer = customer;
    add(selfLink);
    add(linkTo(methodOn(BookController.class).getBooksByСustomerID(customer.getId()))
        .withRel("books"));
  }

  public Long getCustomerId() {
    return customer.getId();
  }

  public String getSurname() {
    return customer.getSurname();
  }

  public String getName() {
    return customer.getName();
  }

  public String getEmail() {
    return customer.getEmail();
  }

  public String getCity() {
    if (customer.getCity() == null) {
      return "";
    }
    return customer.getCity().getCity();
  }

  public String getStreet() {
    return customer.getStreet();
  }

  public String getApartment() {
    return customer.getApartment();
  }
}
