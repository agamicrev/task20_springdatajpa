package com.agamicrev.DTO;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.agamicrev.controller.CustomerController;
import com.agamicrev.domain.Book;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class BookDTO extends ResourceSupport {

  Book book;

  public BookDTO(Book book, Link selfLink) throws NoSuchBookException, NoSuchCustomerException {
    this.book = book;
    add(selfLink);
    add(linkTo(methodOn(CustomerController.class).getCustomersByBookID(book.getId()))
        .withRel("customers"));
  }

  public Long getBookId() {
    return book.getId();
  }

  public String getBookName() {
    return book.getBookName();
  }

  public String getAuthor() {
    return book.getAuthor();
  }

  public String getPublisher() {
    return book.getPublisher();
  }

  public Integer getImprintYear() {
    return book.getImprintYear();
  }

  public Integer getAmount() {
    return book.getAmount();
  }
}
