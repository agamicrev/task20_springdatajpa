package com.agamicrev.DTO;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.agamicrev.controller.CustomerController;
import com.agamicrev.domain.City;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCityException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class CityDTO extends ResourceSupport {

  City city;

  public CityDTO(
      City city, Link selfLink)
      throws NoSuchCustomerException, NoSuchBookException, NoSuchCityException {
    this.city = city;
    add(selfLink);
    add(linkTo(methodOn(CustomerController.class).getCustomersByCityID(city.getId()))
        .withRel("customers"));
  }

  public Long getCityId() {
    return city.getId();
  }

  public String getCity() {
    return city.getCity();
  }
}
