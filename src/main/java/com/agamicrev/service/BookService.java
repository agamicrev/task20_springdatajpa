package com.agamicrev.service;

import com.agamicrev.Repository.BookRepository;
import com.agamicrev.Repository.CustomerRepository;
import com.agamicrev.domain.Book;
import com.agamicrev.domain.Customer;
import com.agamicrev.exceptions.ExistsCustomerForBookException;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

  @Autowired
  BookRepository bookRepository;

  @Autowired
  CustomerRepository customerRepository;

  public Set<Book> getBooksByCustomerId(Long customer_id) throws NoSuchCustomerException {
    Customer customer = customerRepository.findById(customer_id).get();
    if (customer == null) {
      throw new NoSuchCustomerException();
    }
    return customer.getBooks();
  }

  public Book getBook(Long book_id) throws NoSuchBookException {
    Book book = bookRepository.findById(book_id).get();
    if (book == null) {
      throw new NoSuchBookException();
    }
    return book;
  }

  public List<Book> getAllBooks() {
    return bookRepository.findAll();
  }

  @Transactional
  public void createBook(Book book) {
    bookRepository.save(book);
  }

  @Transactional
  public void updateBook(Book uBook, Long book_id) throws NoSuchBookException {
    Book book = bookRepository.findById(book_id).get();
    if (book == null) {
      throw new NoSuchBookException();
    }
    //update
    book.setBookName(uBook.getBookName());
    book.setAuthor(uBook.getAuthor());
    book.setPublisher(uBook.getPublisher());
    book.setImprintYear(uBook.getImprintYear());
    book.setAmount(uBook.getAmount());
  }

  @Transactional
  public void deleteBook(Long book_id) throws NoSuchBookException, ExistsCustomerForBookException {
    Book book = bookRepository.findById(book_id).get();

    if (book == null) {
      throw new NoSuchBookException();
    }
    if (book.getCustomers().size() != 0) {
      throw new ExistsCustomerForBookException();
    }
    bookRepository.delete(book);
  }
}
