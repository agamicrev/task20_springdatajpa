package com.agamicrev.service;

import com.agamicrev.Repository.BookRepository;
import com.agamicrev.Repository.CityRepository;
import com.agamicrev.Repository.CustomerRepository;
import com.agamicrev.domain.Book;
import com.agamicrev.domain.City;
import com.agamicrev.domain.Customer;
import com.agamicrev.exceptions.AlreadyExistsBookInCustomerException;
import com.agamicrev.exceptions.BookAbsentException;
import com.agamicrev.exceptions.CustomerHasNotBookException;
import com.agamicrev.exceptions.ExistsBooksForCustomerException;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCityException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

  @Autowired
  CustomerRepository customerRepository;

  @Autowired
  CityRepository cityRepository;

  @Autowired
  BookRepository bookRepository;

  public List<Customer> getCustomerByCityId(Long city_id) throws NoSuchCityException {
    City city = cityRepository.findById(city_id).get();
    if (city == null) {
      throw new NoSuchCityException();
    }
    return city.getCustomers();
  }

  public Customer getCustomer(Long customer_id) throws NoSuchCustomerException {
    Customer customer = customerRepository.findById(customer_id).get();
    if (customer == null) {
      throw new NoSuchCustomerException();
    }
    return customer;
  }

  public List<Customer> getAllCustomers() {
    return customerRepository.findAll();
  }

  public Set<Customer> getCustomersByBookId(Long book_id) throws NoSuchBookException {
    Book book = bookRepository.findById(book_id).get();
    if (book == null) {
      throw new NoSuchBookException();
    }
    return book.getCustomers();
  }

  @Transactional
  public void createCustomer(Customer customer, Long city_id) throws NoSuchCityException {
    if (city_id > 0) {
      City city = cityRepository.findById(city_id).get();
      if (city == null) {
        throw new NoSuchCityException();
      }
      customer.setCity(city);
    }
    customerRepository.save(customer);
  }

  @Transactional
  public void updateCustomer(Customer uCustomer, Long customer_id, Long city_id)
      throws NoSuchCityException, NoSuchCustomerException {
    City city = cityRepository.findById(city_id).get();
    if (city_id > 0) {
      if (city == null) {
        throw new NoSuchCityException();
      }
    }
    Customer customer = customerRepository.findById(customer_id).get();
    if (customer == null) {
      throw new NoSuchCustomerException();
    }
    //update
    customer.setSurname(uCustomer.getSurname());
    customer.setName(uCustomer.getName());
    customer.setEmail(uCustomer.getEmail());
    if (city_id > 0) {
      customer.setCity(city);
    } else {
      customer.setCity(null);
    }
    customer.setStreet(uCustomer.getStreet());
    customer.setApartment(uCustomer.getApartment());
    customerRepository.save(customer);
  }

  @Transactional
  public void deleteCustomer(Long customer_id)
      throws NoSuchCustomerException, ExistsBooksForCustomerException {
    Customer customer = customerRepository.findById(customer_id).get();
    if (customer == null) {
      throw new NoSuchCustomerException();
    }
    if (customer.getBooks().size() != 0) {
      throw new ExistsBooksForCustomerException();
    }
    customerRepository.delete(customer);
  }

  @Transactional
  public void addBookForCustomer(Long customer_id, Long book_id)
      throws NoSuchCustomerException, NoSuchBookException, AlreadyExistsBookInCustomerException, BookAbsentException {
    Customer customer = customerRepository.findById(customer_id).get();
    if (customer == null) {
      throw new NoSuchCustomerException();
    }

    Book book = bookRepository.findById(book_id).get();
    if (book == null) {
      throw new NoSuchBookException();
    }
    if (customer.getBooks().contains(book) == true) {
      throw new AlreadyExistsBookInCustomerException();
    }
    if (book.getAmount() <= book.getCustomers().size()) {
      throw new BookAbsentException();
    }
    customer.getBooks().add(book);
    customerRepository.save(customer);
  }

  @Transactional
  public void removeBookForCustomer(Long customer_id, Long book_id)
      throws NoSuchCustomerException, NoSuchBookException, CustomerHasNotBookException {
    Customer customer = customerRepository.findById(customer_id).get();
    if (customer == null) {
      throw new NoSuchCustomerException();
    }
    Book book = bookRepository.findById(book_id).get();
    if (book == null) {
      throw new NoSuchBookException();
    }
    if (customer.getBooks().contains(book) == false) {
      throw new CustomerHasNotBookException();
    }
    customer.getBooks().remove(book);
    customerRepository.save(customer);
  }
}
