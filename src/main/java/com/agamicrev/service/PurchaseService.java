package com.agamicrev.service;

import com.agamicrev.Repository.PurchaseRepository;
import com.agamicrev.domain.Purchase;
import com.agamicrev.exceptions.NoSuchPurchaseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseService {

  @Autowired
  PurchaseRepository purchaseRepository;

  public List<Purchase> getAllPurchase() {
    return purchaseRepository.findAll();
  }

  public List<Purchase> getPurchaseFilterBySurname(String like) {
    return purchaseRepository.findByCustomerLike(like + "%");
  }

  public Purchase getPurchase(Long purchase_id) throws NoSuchPurchaseException {
    Purchase purchase = purchaseRepository.findById(purchase_id).get();
    if (purchase == null) {
      throw new NoSuchPurchaseException();
    }
    return purchase;
  }
}
