package com.agamicrev.service;

import com.agamicrev.Repository.CityRepository;
import com.agamicrev.Repository.CustomerRepository;
import com.agamicrev.domain.City;
import com.agamicrev.exceptions.ExistsCustomersForCityException;
import com.agamicrev.exceptions.NoSuchCityException;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService {

  @Autowired
  CityRepository cityRepository;
  private boolean ascending;

  @Autowired
  CustomerRepository customerRepository;

  public List<City> getAllCity() {
    return cityRepository.findAll();
  }

  public City getCity(Long city_id) throws NoSuchCityException {
    City city = cityRepository.findById(city_id).get();
    if (city == null) {
      throw new NoSuchCityException();
    }
    return city;
  }

  @Transactional
  public void createCity(City city) {
    cityRepository.save(city);
  }

  @Transactional
  public void updateCity(City uCity, Long city_id) throws NoSuchCityException {
    City city = cityRepository.findById(city_id).get();

    if (city == null) {
      throw new NoSuchCityException();
    }
    city.setCity(uCity.getCity());
    cityRepository.save(city);
  }

  @Transactional
  public void deleteCity(Long city_id) throws NoSuchCityException, ExistsCustomersForCityException {
    City city = cityRepository.findById(city_id).get();
    if (city == null) {
      throw new NoSuchCityException();
    }
    if (city.getCustomers().size() != 0) {
      throw new ExistsCustomersForCityException();
    }
    cityRepository.delete(city);
  }

}
