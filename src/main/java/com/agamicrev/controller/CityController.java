package com.agamicrev.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.agamicrev.DTO.CityDTO;
import com.agamicrev.domain.City;
import com.agamicrev.exceptions.ExistsCustomersForCityException;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCityException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import com.agamicrev.service.CityService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CityController {

  @Autowired
  CityService cityService;

  @GetMapping(value = "/api/city")
  public ResponseEntity<List<CityDTO>> getAllCities()
      throws NoSuchCustomerException, NoSuchBookException, NoSuchCityException {
    List<City> cityList = cityService.getAllCity();
    Link link = linkTo(methodOn(CityController.class).getAllCities()).withSelfRel();

    List<CityDTO> citiesDTO = new ArrayList<>();
    for (City entity : cityList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      CityDTO dto = new CityDTO(entity, selfLink);
      citiesDTO.add(dto);
    }

    return new ResponseEntity<>(citiesDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/city/{city_id}")
  public ResponseEntity<CityDTO> getCity(@PathVariable Long city_id)
      throws NoSuchCityException, NoSuchCustomerException, NoSuchBookException {
    City city = cityService.getCity(city_id);
    Link link = linkTo(methodOn(CityController.class).getCity(city_id)).withSelfRel();

    CityDTO cityDTO = new CityDTO(city, link);

    return new ResponseEntity<>(cityDTO, HttpStatus.OK);
  }

  @PostMapping(value = "/api/city/{city_id}")
  public ResponseEntity<CityDTO> addCity(@RequestBody City newCity)
      throws NoSuchCityException, NoSuchCustomerException, NoSuchBookException {
    cityService.createCity(newCity);
    Link link = linkTo(methodOn(CityController.class).getCity(newCity.getId())).withSelfRel();

    CityDTO cityDTO = new CityDTO(newCity, link);

    return new ResponseEntity<>(cityDTO, HttpStatus.CREATED);
  }

  @PutMapping(value = "/api/city/{city_id}")
  public ResponseEntity<CityDTO> updateCity(@RequestBody City ucity, @PathVariable Long city_id)
      throws NoSuchCityException, NoSuchCustomerException, NoSuchBookException {
    cityService.updateCity(ucity, city_id);
    City city = cityService.getCity(city_id);
    Link link = linkTo(methodOn(CityController.class).getCity(city_id)).withSelfRel();

    CityDTO cityDTO = new CityDTO(city, link);

    return new ResponseEntity<>(cityDTO, HttpStatus.OK);
  }

  @DeleteMapping(value = "/api/city/{city_id}")
  public ResponseEntity deleteCity(@PathVariable Long city_id)
      throws NoSuchCityException, ExistsCustomersForCityException {
    cityService.deleteCity(city_id);
    return new ResponseEntity(HttpStatus.OK);
  }
}
