package com.agamicrev.controller;

import com.agamicrev.DTO.MessageDTO;
import com.agamicrev.exceptions.AlreadyExistsBookInCustomerException;
import com.agamicrev.exceptions.BookAbsentException;
import com.agamicrev.exceptions.CustomerHasNotBookException;
import com.agamicrev.exceptions.ExistsBooksForCustomerException;
import com.agamicrev.exceptions.ExistsCustomerForBookException;
import com.agamicrev.exceptions.ExistsCustomersForCityException;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCityException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import com.agamicrev.exceptions.NoSuchPurchaseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

  @ExceptionHandler(NoSuchCityException.class)
  ResponseEntity<MessageDTO> handleNoSuchCityException() {
    return new ResponseEntity<MessageDTO>(new MessageDTO("Such city not found"),
        HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(NoSuchCustomerException.class)
  ResponseEntity<MessageDTO> handleNoSuchCustomerException() {
    return new ResponseEntity<MessageDTO>(new MessageDTO("Such customer not found"),
        HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(NoSuchBookException.class)
  ResponseEntity<MessageDTO> handleNoSuchBookException() {
    return new ResponseEntity<MessageDTO>(new MessageDTO("Such book not found"),
        HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(ExistsCustomersForCityException.class)
  ResponseEntity<MessageDTO> handleExistsCustomersForCityException() {
    return new ResponseEntity<MessageDTO>(
        new MessageDTO("Delete imposible. There are customers for this city"), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(ExistsBooksForCustomerException.class)
  ResponseEntity<MessageDTO> handleExistsBooksForCustomerException() {
    return new ResponseEntity<MessageDTO>(
        new MessageDTO("Delete imposible. There are books for this customer"), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(ExistsCustomerForBookException.class)
  ResponseEntity<MessageDTO> handleExistsCustomersForBookException() {
    return new ResponseEntity<MessageDTO>(
        new MessageDTO("Delete imposible. There are customers for this book"), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(AlreadyExistsBookInCustomerException.class)
  ResponseEntity<MessageDTO> handleAlreadyExistsBookInCustomerExceptionException() {
    return new ResponseEntity<MessageDTO>(
        new MessageDTO("Add imposible. The customer already contain this book"),
        HttpStatus.CONFLICT);
  }

  @ExceptionHandler(BookAbsentException.class)
  ResponseEntity<MessageDTO> handleBookAbsentException() {
    return new ResponseEntity<MessageDTO>(new MessageDTO("Now this book is absent"),
        HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(CustomerHasNotBookException.class)
  ResponseEntity<MessageDTO> handleCustomerHasNotBookException() {
    return new ResponseEntity<MessageDTO>(new MessageDTO("The customer hasn't this book"),
        HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(NoSuchPurchaseException.class)
  ResponseEntity<MessageDTO> handleNoSuchPurchaseException() {
    return new ResponseEntity<MessageDTO>(new MessageDTO("Such purchase not found"),
        HttpStatus.NOT_FOUND);
  }

}
