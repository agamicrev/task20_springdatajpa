package com.agamicrev.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.agamicrev.DTO.BookDTO;
import com.agamicrev.domain.Book;
import com.agamicrev.exceptions.ExistsCustomerForBookException;
import com.agamicrev.exceptions.NoSuchBookException;
import com.agamicrev.exceptions.NoSuchCustomerException;
import com.agamicrev.service.BookService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

  @Autowired
  BookService bookService;

  @GetMapping(value = "/api/book/customer/{customer_id}")
  public ResponseEntity<List<BookDTO>> getBooksByСustomerID(@PathVariable Long customer_id)
      throws NoSuchCustomerException, NoSuchBookException {
    Set<Book> bookList = bookService.getBooksByCustomerId(customer_id);
    Link link = linkTo(methodOn(BookController.class).getAllBooks()).withSelfRel();

    List<BookDTO> booksDTO = new ArrayList<>();
    for (Book entity : bookList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      BookDTO dto = new BookDTO(entity, selfLink);
      booksDTO.add(dto);
    }

    return new ResponseEntity<>(booksDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/book/{book_id}")
  public ResponseEntity<BookDTO> getBook(@PathVariable Long book_id)
      throws NoSuchBookException, NoSuchCustomerException {
    Book book = bookService.getBook(book_id);
    Link link = linkTo(methodOn(BookController.class).getBook(book_id)).withSelfRel();

    BookDTO bookDTO = new BookDTO(book, link);

    return new ResponseEntity<>(bookDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/book")
  public ResponseEntity<List<BookDTO>> getAllBooks()
      throws NoSuchBookException, NoSuchCustomerException {
    List<Book> bookList = bookService.getAllBooks();
    Link link = linkTo(methodOn(BookController.class).getAllBooks()).withSelfRel();

    List<BookDTO> booksDTO = new ArrayList<>();
    for (Book entity : bookList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      BookDTO dto = new BookDTO(entity, selfLink);
      booksDTO.add(dto);
    }

    return new ResponseEntity<>(booksDTO, HttpStatus.OK);
  }

  @PostMapping(value = "/api/book")
  public ResponseEntity<BookDTO> addBook(@RequestBody Book newBook)
      throws NoSuchBookException, NoSuchCustomerException {
    bookService.createBook(newBook);
    Link link = linkTo(methodOn(BookController.class).getBook(newBook.getId())).withSelfRel();

    BookDTO bookDTO = new BookDTO(newBook, link);

    return new ResponseEntity<>(bookDTO, HttpStatus.CREATED);
  }

  @PutMapping(value = "/api/book/{book_id}")
  public ResponseEntity<BookDTO> updateBook(@RequestBody Book uBook, @PathVariable Long book_id)
      throws NoSuchBookException, NoSuchCustomerException {
    bookService.updateBook(uBook, book_id);
    Book book = bookService.getBook(book_id);
    Link link = linkTo(methodOn(BookController.class).getBook(book_id)).withSelfRel();

    BookDTO bookDTO = new BookDTO(book, link);

    return new ResponseEntity<>(bookDTO, HttpStatus.OK);
  }

  @DeleteMapping(value = "/api/book/{book_id}")
  public ResponseEntity deleteBook(@PathVariable Long book_id)
      throws ExistsCustomerForBookException, NoSuchBookException {
    bookService.deleteBook(book_id);
    return new ResponseEntity(HttpStatus.OK);
  }
}
