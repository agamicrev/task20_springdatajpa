package com.agamicrev.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.agamicrev.DTO.PurchaseDTO;
import com.agamicrev.domain.Purchase;
import com.agamicrev.exceptions.NoSuchPurchaseException;
import com.agamicrev.service.PurchaseService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PurchaseController {

  @Autowired
  PurchaseService purchaseService;

  @GetMapping(value = "/api/purchase")
  public ResponseEntity<List<PurchaseDTO>> getAllPurchase() {
    List<Purchase> purchaseEntities = purchaseService.getAllPurchase();
    Link link = linkTo(methodOn(PurchaseController.class).getAllPurchase()).withSelfRel();

    List<PurchaseDTO> purchasesDTO = new ArrayList<>();
    for (Purchase entity : purchaseEntities) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      PurchaseDTO dto = new PurchaseDTO(entity, selfLink);
      purchasesDTO.add(dto);
    }

    return new ResponseEntity<>(purchasesDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/purchase/{purchase_id}")
  public ResponseEntity<PurchaseDTO> getPurchase(@PathVariable Long purchase_id)
      throws NoSuchPurchaseException {
    Purchase purchase = purchaseService.getPurchase(purchase_id);
    Link link = linkTo(methodOn(PurchaseController.class).getPurchase(purchase_id)).withSelfRel();

    PurchaseDTO purchaseDTO = new PurchaseDTO(purchase, link);

    return new ResponseEntity<>(purchaseDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/purchase/filter/surname/{surname}")
  public ResponseEntity<List<PurchaseDTO>> getPurchaseFilterBySurname(
      @PathVariable String surname) {
    List<Purchase> purchaseEntities = purchaseService.getPurchaseFilterBySurname(surname);
    Link link = linkTo(methodOn(PurchaseController.class).getAllPurchase()).withSelfRel();

    List<PurchaseDTO> purchasesDTO = new ArrayList<>();
    for (Purchase entity : purchaseEntities) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      PurchaseDTO dto = new PurchaseDTO(entity, selfLink);
      purchasesDTO.add(dto);
    }

    return new ResponseEntity<>(purchasesDTO, HttpStatus.OK);
  }
}

