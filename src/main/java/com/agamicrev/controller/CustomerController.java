package com.agamicrev.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.agamicrev.DTO.CustomerDTO;
import com.agamicrev.domain.Customer;
import com.agamicrev.exceptions.*;
import com.agamicrev.service.CustomerService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

  @Autowired
  CustomerService сustomerService;

  @GetMapping(value = "/api/сustomer/city/{city_id}")
  public ResponseEntity<List<CustomerDTO>> getCustomersByCityID(@PathVariable Long city_id)
      throws NoSuchCityException, NoSuchCustomerException, NoSuchBookException {
    List<Customer> сustomerList = сustomerService.getCustomerByCityId(city_id);

    Link link = linkTo(methodOn(CustomerController.class).getAllCustomers()).withSelfRel();

    List<CustomerDTO> сustomersDTO = new ArrayList<>();
    for (Customer entity : сustomerList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      CustomerDTO dto = new CustomerDTO(entity, selfLink);
      сustomersDTO.add(dto);
    }

    return new ResponseEntity<>(сustomersDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/сustomer/{сustomer_id}")
  public ResponseEntity<CustomerDTO> getCustomer(@PathVariable Long сustomer_id)
      throws NoSuchCustomerException, NoSuchBookException {
    Customer сustomer = сustomerService.getCustomer(сustomer_id);
    Link link = linkTo(methodOn(CustomerController.class).getCustomer(сustomer_id)).withSelfRel();

    CustomerDTO сustomerDTO = new CustomerDTO(сustomer, link);

    return new ResponseEntity<>(сustomerDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/сustomer")
  public ResponseEntity<List<CustomerDTO>> getAllCustomers()
      throws NoSuchCustomerException, NoSuchBookException {
    List<Customer> сustomerList = сustomerService.getAllCustomers();
    Link link = linkTo(methodOn(CustomerController.class).getAllCustomers()).withSelfRel();

    List<CustomerDTO> сustomersDTO = new ArrayList<>();
    for (Customer entity : сustomerList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      CustomerDTO dto = new CustomerDTO(entity, selfLink);
      сustomersDTO.add(dto);
    }

    return new ResponseEntity<>(сustomersDTO, HttpStatus.OK);
  }

  @GetMapping(value = "/api/сustomer/book/{book_id}")
  public ResponseEntity<List<CustomerDTO>> getCustomersByBookID(@PathVariable Long book_id)
      throws NoSuchBookException, NoSuchCustomerException {
    Set<Customer> сustomerList = сustomerService.getCustomersByBookId(book_id);
    Link link = linkTo(methodOn(CustomerController.class).getAllCustomers()).withSelfRel();

    List<CustomerDTO> сustomersDTO = new ArrayList<>();
    for (Customer entity : сustomerList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
      CustomerDTO dto = new CustomerDTO(entity, selfLink);
      сustomersDTO.add(dto);
    }

    return new ResponseEntity<>(сustomersDTO, HttpStatus.OK);
  }

  @PostMapping(value = "/api/сustomer/city/{city_id}")
  public ResponseEntity<CustomerDTO> addCustomer(@RequestBody Customer newCustomer,
      @PathVariable Long city_id)
      throws NoSuchCityException, NoSuchCustomerException, NoSuchBookException {
    сustomerService.createCustomer(newCustomer, city_id);
    Link link = linkTo(methodOn(CustomerController.class).getCustomer(newCustomer.getId()))
        .withSelfRel();

    CustomerDTO сustomerDTO = new CustomerDTO(newCustomer, link);

    return new ResponseEntity<>(сustomerDTO, HttpStatus.CREATED);
  }

  @PutMapping(value = "/api/сustomer/{сustomer_id}/city/{city_id}")
  public ResponseEntity<CustomerDTO> updateCustomer(@RequestBody Customer uCustomer,
      @PathVariable Long сustomer_id, @PathVariable Long city_id)
      throws NoSuchCityException, NoSuchCustomerException, NoSuchBookException {
    сustomerService.updateCustomer(uCustomer, сustomer_id, city_id);
    Customer сustomer = сustomerService.getCustomer(сustomer_id);
    Link link = linkTo(methodOn(CustomerController.class).getCustomer(сustomer_id)).withSelfRel();

    CustomerDTO сustomerDTO = new CustomerDTO(сustomer, link);

    return new ResponseEntity<>(сustomerDTO, HttpStatus.OK);
  }

  @DeleteMapping(value = "/api/сustomer/{сustomer_id}")
  public ResponseEntity deleteCustomer(@PathVariable Long сustomer_id)
      throws NoSuchCustomerException, ExistsBooksForCustomerException {
    сustomerService.deleteCustomer(сustomer_id);
    return new ResponseEntity(HttpStatus.OK);
  }

  @PostMapping(value = "/api/сustomer/{сustomer_id}/book/{book_id}")
  public ResponseEntity<CustomerDTO> addBookForCustomer(@PathVariable Long сustomer_id,
      @PathVariable Long book_id)
      throws NoSuchCustomerException, NoSuchBookException, AlreadyExistsBookInCustomerException, BookAbsentException {
    сustomerService.addBookForCustomer(сustomer_id, book_id);
    Customer сustomer = сustomerService.getCustomer(сustomer_id);
    Link link = linkTo(methodOn(CustomerController.class).getCustomer(сustomer_id)).withSelfRel();

    CustomerDTO сustomerDTO = new CustomerDTO(сustomer, link);

    return new ResponseEntity<>(сustomerDTO, HttpStatus.OK);
  }

  @DeleteMapping(value = "/api/сustomer/{сustomer_id}/book/{book_id}")
  public ResponseEntity<CustomerDTO> removeBookForCustomer(@PathVariable Long сustomer_id,
      @PathVariable Long book_id)
      throws NoSuchCustomerException, NoSuchBookException, CustomerHasNotBookException {
    сustomerService.removeBookForCustomer(сustomer_id, book_id);
    Customer сustomer = сustomerService.getCustomer(сustomer_id);
    Link link = linkTo(methodOn(CustomerController.class).getCustomer(сustomer_id)).withSelfRel();

    CustomerDTO сustomerDTO = new CustomerDTO(сustomer, link);

    return new ResponseEntity<>(сustomerDTO, HttpStatus.OK);
  }

}

